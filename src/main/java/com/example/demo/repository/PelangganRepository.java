package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Pelanggan;

public interface PelangganRepository extends JpaRepository<Pelanggan, Long>{
	
	@Query(value = "SELECT * FROM pelanggan WHERE is_active = true", nativeQuery = true)
	List<Pelanggan> findByIsActive();

}
