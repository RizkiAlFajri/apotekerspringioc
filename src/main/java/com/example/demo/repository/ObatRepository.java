package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Obat;


public interface ObatRepository extends JpaRepository<Obat, Long>{
	
	@Query(value = "SELECT * FROM obat WHERE is_active = true", nativeQuery = true)
	List<Obat> findByIsActive();
	

}
