package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Penjualan;

public interface PenjualanRepository extends JpaRepository<Penjualan, Long>{
	
	@Query(value = "SELECT * FROM penjualan WHERE is_active = true", nativeQuery = true)
	List<Penjualan> findByIsActive();
	
}
