package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;


import com.example.demo.model.Pelanggan;
import com.example.demo.repository.PelangganRepository;

@Service
public class PelangganService {
	private final PelangganRepository pelangganRepository;
	
	public PelangganService(PelangganRepository pelangganRepository) {
		this.pelangganRepository = pelangganRepository;
	}
	
	public List<Pelanggan> getAllPelanggan() {
		return pelangganRepository.findByIsActive();
	}

	public Optional<Pelanggan> findPelangganById(Long id) {
		return pelangganRepository.findById(id);
	}

	public Pelanggan savePelanggan(Pelanggan pelanggan) {
		return pelangganRepository.save(pelanggan);
	}

	public void deletePelanggan(Long id) {
		pelangganRepository.findById(id);
	}

}
