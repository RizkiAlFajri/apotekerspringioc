package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Obat;
import com.example.demo.repository.ObatRepository;

@Service
public class ObatService {
	private final ObatRepository obatRepository;

	public ObatService(ObatRepository obatRepository) {
		this.obatRepository = obatRepository;
	}

	public List<Obat> getAllObat() {
		return obatRepository.findByIsActive();
	}

	public Optional<Obat> findObatById(Long id) {
		return obatRepository.findById(id);
	}

	public Obat saveObat(Obat obat) {
		return obatRepository.save(obat);
	}

	public void deleteObat(Long id) {
		obatRepository.findById(id);
	}

}
