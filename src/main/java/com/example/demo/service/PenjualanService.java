package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Penjualan;
import com.example.demo.repository.PenjualanRepository;

@Service
public class PenjualanService {
	private final PenjualanRepository penjualanRepository;

	public PenjualanService(PenjualanRepository penjualanRepository) {
		this.penjualanRepository = penjualanRepository;
	}

	public List<Penjualan> getAllPenjualan() {
		return penjualanRepository.findByIsActive();
	}

	public Optional<Penjualan> findPenjualanById(Long id) {
		return penjualanRepository.findById(id);
	}

	public Penjualan savePenjualan(Penjualan penjualan) {
		return penjualanRepository.save(penjualan);
	}

	public void deletePenjualan(Long id) {
		penjualanRepository.findById(id);
	}

	public Double getAveragePenjualan() {
		return penjualanRepository.findAll().stream().mapToLong(Penjualan::getTotalBayar).average().orElse(0.0);
	}

}
