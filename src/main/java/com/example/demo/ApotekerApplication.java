package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApotekerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApotekerApplication.class, args);
	}

}
