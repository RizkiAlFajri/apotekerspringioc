package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Obat;

import com.example.demo.service.ObatService;

@RestController
@RequestMapping("/master/")
public class ObatApiController {
	private final ObatService obatService;
	
	public ObatApiController(ObatService obatService) {
		this.obatService = obatService;
	}
	
	@GetMapping("/obat")
	public List<Obat> getAllObat() {
		return obatService.getAllObat();
	}

	@GetMapping("/obat/{id}")
	public ResponseEntity<Obat> findObatById(@PathVariable Long id) {
		Optional<Obat> obat = obatService.findObatById(id);
		return obat.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping("/obat/add")
	public Obat saveObat(@RequestBody Obat obat) {
		return obatService.saveObat(obat);
	}
	
	@PostMapping("/obat/{id}")
	  public void deleteObat(@PathVariable Long id) {
		obatService.deleteObat(id);
	  }
	

}
