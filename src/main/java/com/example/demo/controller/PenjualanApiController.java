package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Penjualan;
import com.example.demo.service.PenjualanService;

@RestController
@RequestMapping("/transaction/")
public class PenjualanApiController {
	private final PenjualanService penjualanService;

	public PenjualanApiController(PenjualanService penjualanService) {
		this.penjualanService = penjualanService;
	}

	@GetMapping("/penjualan")
	public List<Penjualan> getAllPenjualan() {
		return penjualanService.getAllPenjualan();
	}

	@GetMapping("/penjualan/{id}")
	public ResponseEntity<Penjualan> findPenjualanById(@PathVariable Long id) {
		Optional<Penjualan> penjualan = penjualanService.findPenjualanById(id);
		return penjualan.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping("/penjualan/add")
	public Penjualan savePenjualan(@RequestBody Penjualan penjualan) {
		return penjualanService.savePenjualan(penjualan);
	}
	
	@PostMapping("/penjualan/{id}")
	  public void deletePenjualan(@PathVariable Long id) {
		penjualanService.deletePenjualan(id);
	  }

	  @GetMapping("/penjualan/avg-penjualan")
	  public Double getAveragePenjualan() {
	    return penjualanService.getAveragePenjualan();
	  }

}
