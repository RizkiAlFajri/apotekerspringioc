package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Pelanggan;

import com.example.demo.service.PelangganService;


@RestController
@RequestMapping("/master/")
public class PelangganApiController {
	private final PelangganService pelangganService;

	public PelangganApiController(PelangganService pelangganService) {
		this.pelangganService = pelangganService;
	}

	@GetMapping("/pelanggan")
	public List<Pelanggan> getAllPelanggan() {
		return pelangganService.getAllPelanggan();
	}

	@GetMapping("/pelanggan/{id}")
	public ResponseEntity<Pelanggan> findPelangganById(@PathVariable Long id) {
		Optional<Pelanggan> pelanggan = pelangganService.findPelangganById(id);
		return pelanggan.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping("/pelanggan/add")
	public Pelanggan savePelanggan(@RequestBody Pelanggan pelanggan) {
		return pelangganService.savePelanggan(pelanggan);
	}
	
	@PostMapping("/pelanggan/{id}")
	  public void deletePelanggan(@PathVariable Long id) {
		pelangganService.deletePelanggan(id);
	  }
	

}
