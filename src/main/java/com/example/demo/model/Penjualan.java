package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="obat")
public class Penjualan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_pelanggan")
	private Long idPelanggan;
	
	@Column(name = "id_obat")
	private String idObat;
	
	@Column(name = "tanggal")
	private Date tanggal;
	
	@Column(name = "jumlah_obat")//beli berapa
	private Long jumlahObat;
	
	@Column(name = "total")//harga obat*jumlah obat
	private String total;
	
	@Column(name = "total_bayar")
	private Long totalBayar;
	
	@Column(name = "is_active")
	public Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "id_pelanggan", insertable = false, updatable = false)
	private Pelanggan pelanggan;
	
	@ManyToOne
	@JoinColumn(name = "id_obat", insertable = false, updatable = false)
	private Obat obat;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPelanggan() {
		return idPelanggan;
	}

	public void setIdPelanggan(Long idPelanggan) {
		this.idPelanggan = idPelanggan;
	}

	public String getIdObat() {
		return idObat;
	}

	public void setIdObat(String idObat) {
		this.idObat = idObat;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public Long getJumlahObat() {
		return jumlahObat;
	}

	public void setJumlahObat(Long jumlahObat) {
		this.jumlahObat = jumlahObat;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public Long getTotalBayar() {
		return totalBayar;
	}

	public void setTotalBayar(Long totalBayar) {
		this.totalBayar = totalBayar;
	}

	public Pelanggan getPelanggan() {
		return pelanggan;
	}

	public void setPelanggan(Pelanggan pelanggan) {
		this.pelanggan = pelanggan;
	}

	public Obat getObat() {
		return obat;
	}

	public void setObat(Obat obat) {
		this.obat = obat;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	
	

}
